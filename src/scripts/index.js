var swiper = new Swiper('.swiper-container', {
    direction: 'vertical',
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
});

$('#circle').circleProgress({
    value: 0.99,
    size: 250,
    fill: {
        gradient: ["#e08606", "#bfbf00"]
    }
});

console.log(screenTop);

